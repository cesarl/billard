class Ball
        constructor: (@x, @y, @r) ->
                @top = @y - @r
                @bottom = @y + @r
                @left = @x - @r
                @right = @x + @r
                @vx = @vy = 0
                @nx = @x
                @ny = @y
                @color = "#000000"
                @handled = false;
                @maxSpeed = 5
                @
        getRandomPosition: (minX, minY, maxX, maxY) ->
                @x = Math.floor(Math.random() * (maxX - minX + 1) + minX)
                @y = Math.floor(Math.random() * (maxY - minY + 1) + minY)
                @nx = @x
                @ny = @y
                @updateSquare()
        collideBall: (b) ->
            d2 = (@nx - b.nx) * (@nx - b.nx) + (@ny - b.ny) * (@ny - b.ny)
            if d2 > (@r + b.r) * (@r + b.r) then return d2 else return true
        bounceBall: (b2) ->
                if @handled then mass1 = 30 else mass1 = @r
                if b2.handled then mass2 = 30 else mass2 = b2.r
                dx = @nx - b2.nx
                dy = @ny - b2.ny
                angle = Math.atan2 dy, dx
                dir1 = Math.atan2 @vy, @vx
                dir2 = Math.atan2 b2.vy, b2.vx
                sp1 = Math.sqrt @vx * @vx + @vy * @vy
                sp2 = Math.sqrt b2.vx * b2.vx + b2.vy * b2.vy
                vx1 = sp1 * Math.cos dir1 - angle
                vy1 = sp1 * Math.sin dir1 - angle
                vx2 = sp2 * Math.cos dir2 - angle
                vy2 = sp2 * Math.sin dir2 - angle
                fvx1 = ((mass1 - mass2) * vx1 + (mass2 + mass2) * vx2) / (mass1 + mass2)
                fvx2 = ((mass1 + mass1) * vx1 + (mass2 - mass1) * vx2) / (mass1 + mass2)
                fvy1 = vy1
                fvy2 = vy2
                @vx = Math.cos(angle) * fvx1 + Math.cos(angle + Math.PI/2) * fvy1
                @vy = Math.sin(angle) * fvx1 + Math.sin(angle + Math.PI/2) * fvy1
                b2.vx = Math.cos(angle) * fvx2 + Math.cos(angle + Math.PI/2) * fvy2
                b2.vy = Math.sin(angle) * fvx2 + Math.sin(angle + Math.PI/2) * fvy2
                @nx += @vx
                @ny += @vy
                b2.nx += b2.vx
                b2.ny += b2.vy

        outsideRect: (x, y, xx, yy) ->
                res = col: false, effect: {}
                if @top < y
                        res.col = true
                        res.effect.ny = y + @r
                        res.effect.vy = @vy * -1
                        res.effect.vy = res.effect.vy - res.effect.vy * 0.5
                else if @bottom > yy
                        res.col = true
                        res.effect.ny = yy - @r
                        res.effect.vy = @vy * -1
                        res.effect.vy = res.effect.vy - res.effect.vy * 0.5
                else if @left < x
                        res.col = true
                        res.effect.nx = x + @r
                        res.effect.vx = @vx * -1
                        res.effect.vx = res.effect.vx - res.effect.vx * 0.5
                else if @right > xx
                        res.col = true
                        res.effect.nx = xx - @r
                        res.effect.vx = @vx * -1
                        res.effect.vx = res.effect.vx - res.effect.vx * 0.5
                res
        applyColEffect: (col) ->
                for key, val of col.effect
                        if @[key] then @[key] = val
                @updateSquare()
        newForce: (x, y) ->
                if x then @vx = x
                if y then @vy = y
        updateSquare: () ->
                @top = @ny - @r
                @bottom = @ny + @r
                @left = @nx - @r
                @right = @nx + @r
        update: () ->
                @nx = @x + @vx
                @ny = @y + @vy
                @vx -= @vx * 0.01
                @vy -= @vy * 0.01
                if @handled
                        if Math.abs(@vx + @kx) <= @maxSpeed then @vx += @kx
                        if Math.abs(@vy + @ky) <= @maxSpeed then @vy += @ky
                @updateSquare()
                @
        applyMovement: () ->
                @x = @nx
                @y = @ny
                @
        changeColor: (color) ->
                @color = color
                @
        handle: (@maxSpeed = 5) ->
                @handled = true;
                @kx = 0
                @ky = 0
                @keyDownListener = window.addEventListener "keydown", ( (e) => @keyDown(e)), false
                @keyUpListener = window.addEventListener "keyup", ( (e) => @keyUp(e)), false
                @
        keyDown: (e) ->
                switch e.keyCode
                        when 37 then @kx = -0.2
                        when 38 then @ky = -0.2
                        when 39 then @kx = 0.2
                        when 40 then @ky = 0.2
        keyUp: (e) ->
                switch e.keyCode
                        when 37 then @kx = 0
                        when 38 then @ky = 0
                        when 39 then @kx = 0
                        when 40 then @ky = 0
        collidePoint: (x, y) ->
                if Math.abs(x - @x) <= @r and Math.abs(y - @y) <= @r then return true
                false
class App
        constructor: (canvasId, @w = 700, @h = 400) ->
                @canvas = document.getElementById canvasId
                @ctx = @canvas.getContext "2d"
                @canvas.width = @w
                @canvas.height = @h
                @col = []
                @dragging = {ToF: false, ball: false, event : false}
                @fullCircle = Math.PI * 180 / 2
                @canvas.addEventListener "mousedown", ((e) => @onMouseDown(e)), false
                @_onmouseup = (e) => @onMouseUp(e)
                @_onmousemove = (e) => @onMouseMove(e)
        clear: () ->
                @ctx.clearRect 0, 0, @w, @h
        getRandomBall: () ->
                @col[Math.random() * @col.length >> 0]
        addRandomBall: (nb = 10, minRad = 5, maxRad = 20) ->
                i = 0
                while i < nb
                        tmp = new Ball Math.floor(Math.random() * @w), Math.floor(Math.random() * @h), Math.floor(Math.random() * (maxRad - minRad + 1) + minRad)
                        noplaceanymore = 0
                        while @ballsCollision(tmp) and noplaceanymore < 10
                                tmp.getRandomPosition 0, 0, @w, @h
                                noplaceanymore++
                        @col.push tmp
                        i++
        ballsCollision: (ball) ->
                i = 0
                mi = @col.length
                while i < mi
                        if @col[i] isnt ball and ball.collideBall(@col[i]) is true then return true
                        i++
                if ball.outsideRect(0, 0, @w, @h).col then return true
                false
        applyForceOnCol: (min, max) ->
                @col.forEach (elem, i) =>
                        elem.newForce Math.floor(Math.random() * (max - min + 1) + min), Math.floor(Math.random() * (max - min + 1) + min)
        updateCol: () ->
                @col.forEach (elem, i) =>
                        elem.update()
        animateBall: () ->
                @animation = true
                anim = =>
                        mj = @col.length
                        @clear()
                        @col.forEach (elem, i) =>
                                elem.update()
                        @col.forEach (elem, i) =>
                                outrect = elem.outsideRect 0, 0, @w, @h
                                if outrect.col then elem.applyColEffect(outrect)
                        @col.forEach (elem, i) =>
                                j = i + 1
                                while (j < mj)
                                        if elem.collideBall(@col[j]) is true
                                                elem.bounceBall(@col[j])
                                        j++
                        @col.forEach (elem, i) =>
                                elem.applyMovement()
                                if elem.color isnt "#000000" then @ctx.fillStyle = elem.color
                                @ctx.beginPath()
                                @ctx.arc elem.x, elem.y, elem.r, 0, @fullCircle, false
                                @ctx.fill()
                                if elem.color isnt "#000000" then @ctx.fillStyle = "#000000"
                        if @animation then window.requestAnimFrame(=> anim())
                anim()
        onMouseDown: (e) ->
                found = false
                @col.forEach (elem, i) =>
                        if elem.collidePoint(e.x, e.y)
                                @dragging = {ToF: true, ball: elem, event : e}
                                elem.vx = 0
                                elem.vy = 0
                                @canvas.addEventListener "mouseup", @_onmouseup, false
                                @canvas.addEventListener "mousemove", @_onmousemove, false
                                found = true
                if found then return true
                @dragging = {ToF: false, ball: false, event : false}
                false
        onMouseUp: (e) ->
                dx = e.x - @dragging.past.x
                dy = e.y - @dragging.past.y
                @dragging.ball.vx = dx
                @dragging.ball.vy = dy
                @canvas.removeEventListener 'mouseup', @_onmouseup, false
                @canvas.removeEventListener 'mousemove', @_onmousemove, false
                @
        onMouseMove: (e) ->
                if @dragging.ToF is false then return @
                dx = e.x - @dragging.event.x
                dy = e.y - @dragging.event.y
                @dragging.ball.x += dx
                @dragging.ball.y += dy
                @dragging.past = @dragging.event
                @dragging.event = e
                @
app = new App "canvas"
app.addRandomBall 10
app.applyForceOnCol -30, 30
myBall = app.getRandomBall().changeColor("#caca12").handle(5)
app.animateBall()